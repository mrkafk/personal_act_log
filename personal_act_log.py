#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
The MIT License (MIT)

Copyright (c) 2017 Marcin Krol <mrkafk@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import datetime
import os
import cPickle
import sys
import shutil
import codecs
import logging
import operator
import Queue
import thread
import time
import hashlib
import base64

from ConfigParser import NoOptionError
from collections import OrderedDict
from datetime import timedelta

from Tkinter import Tk, Toplevel, Frame, Entry, Listbox, Button, Label, INSERT,\
                    Checkbutton, OptionMenu, IntVar, StringVar, PhotoImage, END,\
                    SUNKEN, W, CENTER

import tkMessageBox
import tkFileDialog

from icecream import ic

from lib import pyaes

from lib.patricia import trie
from lib.pal_utils import setup_logging, mkdirp, get_config_parser,\
    get_config_dir_portable, populate_config_file_if_empty,\
    RedirectStdToLog, TaskStartTimeTracker, SETTINGS_ICON_BASE64,\
    dump_attr_values, render_hist_entry_content, parse_hist_entry_content,\
    datetime_to_ts, XlsxExport, get_midnight, ts_to_datetime, get_first_on_criterion,\
    unicode_csv_reader, dt_to_task_datestr, dt_change_day, KanboardConnection,\
    get_category_disp_lengths, dt_to_timestamp, datetime_to_date

from sqlalchemy import create_engine, schema, types, Column, Integer, String, desc, ForeignKey, and_, desc
from sqlalchemy.orm import sessionmaker, relationship, joinedload
from sqlalchemy.ext.declarative import declarative_base

from dictalchemy import DictableModel, make_class_dictable


### Globals ###

RECALL_DATA = trie('root')
QR = None
ENTRY_BG_COLOR = '#e3ecfc'


DBFNAME = 'personal_activity_log.db'
ENGINE = None
SESSION = None
DBFPATH = None
Base = None


def progfile_dir():
    return os.path.abspath(os.path.dirname(__file__))

MAINDIR = progfile_dir()
CONFIG_FNAME = 'PersonalActivityLog.conf'
APPDATA = get_config_dir_portable('PersonalActivityLog')
mkdirp(APPDATA)
CONFIG_FPATH = None
LOG_FPATH = os.path.join(APPDATA, 'personal_activity_log.log')
LOG = None
TICKET_SYSTEM_URL = None
TICKET_SYSTEM_AUTH_TOKEN = None
CAT_HIST_DISP_MAXLEN = None

REQUEST_Q = Queue.Queue()
RESULT_Q = Queue.Queue()

STATUS_BAR = None

CONFIG = None
CFG_MAIN = None

MAIN = None

CONFIG_TEMPLATE = '''
[main]

# Minimize on save
minimize_on_save = 1

# Backup history every n saves
backup_hist_every = 3

# When rotating backups every "backup_hist_every" save events, keep this number of copies
keep_backups = 10

log_level = INFO

category_hist_display_lengths = 5, 10, 15, 20, 25, 30
'''


CONFIG_DEFAULTS = {'minimize_on_save': 1, 'backup_hist_every': 3,
                   'keep_backups': 10, 'work_hour_default': '08:00',
                   'cat_disp_len_hist': 10}
WORK_HOUR_CHOICES = OrderedDict()
WORK_HOUR_CHOICES_SEC = OrderedDict()


def init_sqa(savedir):
    global ENGINE, SESSION, DBFPATH, Base
    DBFPATH = os.path.join(APPDATA, DBFNAME)
    ENGINE = create_engine('sqlite:///{}'.format(DBFPATH))
    Base = declarative_base(ENGINE, cls=DictableModel)
    SESSION = sessionmaker(bind=ENGINE)

init_sqa(APPDATA)


class Category(Base):
    __tablename__ = 'category'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    ticket_system_project_id = Column(String)
    ticket_system_url = Column(String)
    tasks = relationship('Task', order_by='Category.id', back_populates='category')


class Task(Base):
    __tablename__ = 'task'
    id = Column(Integer, primary_key=True)
    timestamp = Column(Integer)
    desc = Column(String)

    category_id = Column(Integer, ForeignKey('category.id'))
    category = relationship('Category', back_populates='tasks')

    def get_start_day_midnight_ts(self):
        '''Get timestamp of midnight for the day when the task was done.'''
        return get_midnight(ts_to_datetime(self.timestamp), return_timestamp=True)


class DayStart(Base):
    __tablename__ = 'daystart'
    id = Column(Integer, primary_key=True)
    midnight_ts = Column(Integer)
    daystart_sec = Column(Integer)


class ContextSession(object):
    def __init__(self, sesmaker, selectonly=False):
        self.sesmaker = sesmaker
        self.session = None
        self.selectonly = selectonly

    def __enter__(self):
        self.session = self.sesmaker()
        return self.session

    def __exit__(self, exc_type, exc_val, exc_tb):
        if not self.selectonly:
            self.session.commit()
        self.session.close()


def upsert_task(session, task_timestamp, desc):
    task = session.query(Task).filter(Task.timestamp == task_timestamp).one_or_none()
    if not task:
        LOG.info('Adding task "%s" for timestamp %s', desc, task_timestamp)
        task = Task(timestamp=task_timestamp, desc=desc)
    else:
        LOG.info('Updating task "%s" for timestamp %s', task.desc, task.timestamp)
        task.desc = desc
    session.add(task)


def convert_pickle(pickle_fpath):
    tlog = {}
    with open(pickle_fpath, 'rb') as fo:
        tlog = cPickle.load(fo)
    with ContextSession(SESSION) as session:
        for task_timestamp, desc in tlog.items():
            upsert_task(session, task_timestamp, desc)


def import_tasks_csv(fpath):
    with codecs.open(fpath, encoding='utf8', errors='ignore') as fo:
        reader = unicode_csv_reader(fo)
        header = next(reader)
        pos_timestamp = header.index('timestamp')
        pos_desc = header.index('desc')
        with ContextSession(SESSION) as session:
            for line in reader:
                timestamp = int(line[pos_timestamp])
                desc = line[pos_desc]
                task = Task(timestamp=timestamp, desc=desc)
                session.add(task)
                print 'Adding task', timestamp, desc
        sys.exit(0)


def get_aes():
    k = hashlib.sha256(CONFIG_FPATH).hexdigest()[:32]
    return pyaes.AESModeOfOperationCTR(k)


def encrypt_str(plaintext):
    aes = get_aes()
    ciphertext = aes.encrypt(plaintext)
    return base64.encodestring(ciphertext)


def decrypt_str(s):
    aes = get_aes()
    ciphertext = base64.decodestring(s)
    plaintext = aes.decrypt(ciphertext)
    return plaintext


if len(sys.argv) > 1:
    LOG = setup_logging()
    opt = sys.argv[1]
    if opt == '-c':
        pass
    elif opt == '--convert-pickle':
        pickle_fpath = sys.argv[2]
        convert_pickle(pickle_fpath)
    elif opt == '--import-tasks-csv':
        import_tasks_csv(sys.argv[2])
else:
    print("Logging to", LOG_FPATH)
    LOG = setup_logging(to_fpath=LOG_FPATH)
    sys.stdout = RedirectStdToLog(LOG.info)
    sys.stderr = RedirectStdToLog(LOG.error)


class QuickRecall(object):
    ''' Recall strings quickly from history based on a typed prefix. '''

    def __init__(self):
        pass

    def populate(self, timelog_values):
        ''' Populate recall trie.'''
        for value in timelog_values:
            RECALL_DATA[value] = None

    def prefixed_alphabetic(self, s):
        '''Get at most one key prefixed by s.
        If there are more than 1 key prefixed by s, sort the keys in alphabetic order
        and return the first one. E.g.:
        - seek all keys prefixed by 'a'
        - there are keys 'aa', 'ab' matching
        - return 'aa'
        '''
        keys = list(RECALL_DATA.iter(s))
        if not keys:
            return ''
        keys.sort()
        return keys[0]

    def add_one_key(self, k):
        ''' Add a single key to recall trie.'''
        RECALL_DATA[k] = None


def get_nearest_start_hr(task_timestamp):
    return get_first_on_criterion(reversed(WORK_HOUR_CHOICES_SEC.values()), lambda ts: ts < task_timestamp)


def upsert_day_start(label_or_ts, midnight_ts=None):
    start_sec = label_or_ts
    if isinstance(label_or_ts, basestring):
        start_sec = WORK_HOUR_CHOICES_SEC[label_or_ts]
    if not midnight_ts:
        midnight_ts = get_midnight(datetime.datetime.now(), return_timestamp=True)
    LOG.debug('Label %s midnight_ts %s', label_or_ts, midnight_ts)
    with ContextSession(SESSION) as session:
        ds = session.query(DayStart).filter(DayStart.midnight_ts == midnight_ts).one_or_none()
        if not ds:
            ds = DayStart()
            ds.midnight_ts = midnight_ts
        ds.daystart_sec = start_sec
        session.add(ds)


def strip_category(content, session):
    categories = [c.name for c in session.query(Category).order_by(Category.name).all()]
    content = content.strip()
    return content



class TimeLog(object):

    ''' Saving activity history to pickle.'''

    def __init__(self, backup_hist_every=3, keep_backups=5):
        self.backup_hist_every = backup_hist_every
        self.keep_backups = abs(keep_backups)
        self.save_counter = 0

    def get_tasks(self, num=100, earlier_or_eq=None, most_recent=True):
        tasks = []
        with ContextSession(SESSION, selectonly=True) as session:
            q = session.query(Task).options(joinedload(Task.category))
            if num == -1:
                tasks = q.all()
            elif earlier_or_eq:
                ts = int(datetime_to_ts(earlier_or_eq))
                tasks = q.filter(Task.timestamp < ts).order_by(desc(Task.timestamp)).limit(num).all()
            elif most_recent:
                tasks = q.order_by(desc(Task.timestamp)).limit(num).all()
            else:
                tasks = q.order_by(Task.timestamp).limit(num).all()
        tasklist = []
        for task in tasks:
            cat_name = ''
            if task.category:
                cat_name = task.category.name
            tasklist.append((task.timestamp, task.desc, cat_name))
        return tasklist

    def get_latest_task_between_timestamps(self, start_ts, end_ts):
        with ContextSession(SESSION, selectonly=True) as session:
            return session.query(Task).filter(and_(Task.timestamp >= start, Task.timestamp < end)).order_by(desc(Task.timestamp)).first()

    def get_categories(self):
        categories = []
        with ContextSession(SESSION, selectonly=True) as session:
            categories = [c.name for c in session.query(Category).order_by(Category.name).all()]
        return categories

    def get_category_by_name(self, name):
        with ContextSession(SESSION, selectonly=True) as session:
            return session.query(Category).filter(Category.name == name).one_or_none()

    def add_category(self, name):
        with ContextSession(SESSION) as session:
            cat = Category(name=name)
            session.add(cat)
            session.flush()
            return cat.id

    def add_task(self, content, task_datetime=None):
        if not task_datetime:
            task_datetime = datetime.datetime.now()
        ts = int(datetime_to_ts(task_datetime))
        task = Task(timestamp=ts, desc=content)
        with ContextSession(SESSION) as session:
            session.add(task)
        return task_datetime

    def get_task_by(self, **kwargs):
        with ContextSession(SESSION, selectonly=True) as session:
            return session.query(Task).options(joinedload(Task.category)).filter_by(**kwargs).one_or_none()

    def set_task_category_by_task_ts_cat_id(self, task_timestamp, category_id):
        with ContextSession(SESSION) as session:
            task = session.query(Task).filter_by(timestamp=task_timestamp).one_or_none()
            cat = session.query(Category).filter_by(id=category_id).one_or_none()
            if task and cat:
                LOG.debug('cat %s task %s', cat, task)
                task.category = cat
                session.add(task)

    def update_task(self, old_timestamp, new_timestamp, content):
        with ContextSession(SESSION) as session:
            task = session.query(Task).filter(Task.timestamp == old_timestamp).one()
            task.timestamp = new_timestamp
            task.desc = content
            session.add(task)

    def periodic_backup(self):
        '''Run backup if relevant.'''
        self.save_counter += 1
        if self.backup_hist_every and self.save_counter == self.backup_hist_every:
            self.save_counter = 0
            self.backup_and_rotate()

    def backup_and_rotate(self):
        ''' Backup existing settings and rotate if relevant.'''
        for i in range(self.keep_backups-1, 0, -1):
            bak_fpath = '{}.{}'.format(DBFPATH, i)
            bak_fpath_next = '{}.{}'.format(DBFPATH, i+1)
            try:
                shutil.copy(bak_fpath, bak_fpath_next)
                LOG.info('Rotating backups: overwrote %s with %s', bak_fpath_next, bak_fpath)
            except IOError as e:
                if e.errno != 2:
                    LOG.error('Err %s', e)
        try:
            src = DBFPATH
            dst = '{}.1'.format(DBFPATH)
            shutil.copy(src, dst)
            LOG.info('Overwrote %s with %s', dst, src)
        except IOError as e:
            LOG.error('Err %s', e)

    def _asdict_all(self, q, attr_as_key, exclude=None):
        all_instances = OrderedDict()
        for obj in q.all():
            d = obj.asdict(exclude=exclude)
            all_instances[d[attr_as_key]] = d
        return all_instances

    def dump_all_data_to_dict(self):
        d = {}
        with ContextSession(SESSION, selectonly=True) as session:
            d['Task'] = self._asdict_all(session.query(Task).order_by(Task.timestamp), 'timestamp')
            d['DayStart'] = self._asdict_all(session.query(DayStart).order_by(DayStart.midnight_ts), 'midnight_ts', exclude=['id'])
        return self.calc_task_durations(self.adjust_day_starts(self.flatten_task_info(d)))

    def flatten_task_info(self, sqa_dump):
        d = OrderedDict()
        day_start_default = WORK_HOUR_CHOICES_SEC[CFG_MAIN.get('work_hour_default') or CONFIG_DEFAULTS.get('work_hour_default', '08:00')]
        d_daystart = sqa_dump['DayStart']
        for task_timestamp, task in sqa_dump['Task'].items():
            row = dict(task)
            midnight = get_midnight(ts_to_datetime(task_timestamp), return_timestamp=True)
            day_start = d_daystart.get(midnight, {'daystart_sec':day_start_default, 'midnight_ts': midnight})
            row.update(day_start)
            d[task_timestamp] = row
        return d

    def adjust_day_starts(self, data_dump):
        '''For days where where daystart_sec is later than this day's earliest task finish timestamp,
           adjust daystart_sec to nearest earlier hour/half hour.
        '''
        # Group tasks by midnight_ts
        by_midnight_ts = OrderedDict()
        for task in data_dump.values():
            by_midnight_ts.setdefault(task['midnight_ts'], []).append(task)
        # Add missing DayStart instances
        for midnight_ts, tasks in by_midnight_ts.items():
            if tasks:
                daystart_sec = tasks[0]['daystart_sec']
                earliest_task_ts = min([t['timestamp'] for t in tasks])
                if earliest_task_ts - midnight_ts < daystart_sec:
                    daystart_sec = get_nearest_start_hr(max(midnight_ts, earliest_task_ts-midnight_ts))
                    for task in tasks:
                        LOG.info('Adjusting day start to %s for task %s', daystart_sec, task['desc'])
                        task['daystart_sec'] = daystart_sec
                upsert_day_start(daystart_sec, midnight_ts=midnight_ts)
        return data_dump

    def calc_task_durations(self, data_dump):
        by_midnight_ts = self.data_by_midnight(data_dump)
        prev_task_finish = None
        for tasklist in by_midnight_ts.values():
            if tasklist:
                prev_task_finish = tasklist[0]['midnight_ts'] + tasklist[0]['daystart_sec']
            for task in tasklist:
                task['task_duration_sec'] = task['timestamp'] - prev_task_finish
                prev_task_finish = task['timestamp']
        return by_midnight_ts

    def data_by_midnight(self, data_dump):
        by_midnight_ts = OrderedDict()
        tasklist = data_dump.values()
        tasklist.sort(key=operator.itemgetter('midnight_ts'))
        for task in tasklist:
            by_midnight_ts.setdefault(task['midnight_ts'], []).append(task)
        for tasklist in by_midnight_ts.values():
            tasklist.sort(key=operator.itemgetter('timestamp'))
        return by_midnight_ts


def get_day_start(dt=None):
    if not dt:
        dt = datetime.datetime.today()
    midnight_today_ts = get_midnight(dt, return_timestamp=True)
    with ContextSession(SESSION) as session:
        ds = session.query(DayStart).filter(DayStart.midnight_ts == midnight_today_ts).one_or_none()
        if ds:
            rev = {v:k for k,v in WORK_HOUR_CHOICES_SEC.items()}
            return rev.get(ds.daystart_sec, '08:00')
    return CFG_MAIN.get('work_hour_default') or CONFIG_DEFAULTS.get('work_hour_default', '08:00')


##### App settings dialog #####


def config_setdefault(setting_name, section='main'):
    try:
        return CONFIG.get(section, setting_name)
    except NoOptionError:
        CONFIG.set(section, setting_name, str(CONFIG_DEFAULTS.get(setting_name, 1)))
    return CONFIG.get(section, setting_name)


def populate_work_hour_choices():
    global WORK_HOUR_CHOICES, WORK_HOUR_CHOICES_SEC
    for sec in range(18000, 82801, 1800):
        h, rem = divmod(sec, 3600)
        m = rem/60
        label = '{0:02d}:{1:02d}'.format(h, m)
        WORK_HOUR_CHOICES[label] = timedelta(0, sec)
        WORK_HOUR_CHOICES_SEC[label] = sec


def add_optmenu_start_work_hour(frame, strvar):
    tkeys = WORK_HOUR_CHOICES.keys()
    tkeys.sort()
    wh_optmenu = OptionMenu(frame, strvar, *tkeys)
    wh_optmenu.config(width=7)
    return wh_optmenu


class TkModalOKCancelDialog(object):
    '''Modal dialog with OK and Cancel buttons.'''

    def __init__(self, parent, root=None, *args, **kwargs):
        self.parent_obj = parent
        self.parent = parent
        if not root:
            self.parent = parent.root
        self.root = Toplevel(self.parent)
        # Override root_configure
        self.root_configure()
        self.root.bind('<Escape>', self.__cb_button_cancel)
        self.root.bind('<Return>', self.__cb_button_ok)
        # self.frame = Frame(self.root)
        self.frame = Frame(self.root, bd=3)
        # self.frame = Frame(self.root, bg='#a4f442')
        self.child_init(*args, **kwargs)
        self.populate_window()
        self.root.grab_set()
        self.root.protocol("WM_DELETE_WINDOW", self.__cb_button_cancel)
        self.root.focus_set()
        self.root.wait_window(self.root)

    def child_init(self, *args, **kwargs):
        '''Set all variables you'd normally set in __init__ here'''
        pass

    def root_configure(self):
        pass

    def populate_window(self):
        pass

    def __cb_button_ok(self, event=None):
        self.parent.focus_set()
        if not self.ok_action():
            self.root.destroy()

    def ok_action(self):
        pass

    def __cb_button_cancel(self, event=None):
        self.cancel_action()
        self.parent.focus_set()
        self.root.destroy()

    def cancel_action(self):
        pass

    def add_OK_Cancel(self, rownum, ok_column=0, cancel_column=1):
        self.button_ok = Button(self.frame, text="OK", command=self.__cb_button_ok)
        self.button_ok.grid(row=rownum, column=ok_column, sticky='sw')
        self.button_cancel = Button(self.frame, text="Cancel", command=self.__cb_button_cancel)
        self.button_cancel.grid(row=rownum, column=cancel_column, sticky='se')


def popupmsg(msg):
    popup = Tk()
    popup.wm_title("!")
    label = Label(popup, text=msg, font=("Verdana", 12))
    label.pack(side="top", fill="x", pady=10)
    B1 = Button(popup, text="Okay", command = popup.destroy)
    B1.pack()
    popup.mainloop()


class TkSettingsDialog(TkModalOKCancelDialog):
    '''App configuration dialog.'''

    def child_init(self):
        self.root.minsize(width=720, height=50)
        self.day_start_default_optmenu = None
        self.mos_var = IntVar()
        self.mos_var.set(int(config_setdefault('minimize_on_save')))
        self.day_start_default_var = StringVar()
        self.conntest_result_var = StringVar()
        self.cat_disp_len_hist_var = IntVar()

    def root_configure(self):
        self.root.rowconfigure(0, weight=1)
        self.root.columnconfigure(0, weight=1)

    def ok_action(self, event=None):
        self.save_settings()
        self.update_current_settings()
        if hasattr(self.parent_obj, 'reconfigure_visit'):
            getattr(self.parent_obj, 'reconfigure_visit')(minimize_on_save=int(config_setdefault('minimize_on_save')))
        if hasattr(self.parent_obj, 'populate_history_listbox'):
            getattr(self.parent_obj, 'populate_history_listbox')()

    def update_current_settings(self):
        global CAT_HIST_DISP_MAXLEN
        CAT_HIST_DISP_MAXLEN = self.cat_disp_len_hist_var.get()

    def populate_window(self):
        self.frame.grid(sticky="nsew")
        # self.frame.grid_columnconfigure(0, weight=1)
        # self.frame.grid_columnconfigure(1, weight=1)
        self.frame.grid_columnconfigure(2, weight=1)
        for i in range(10):
            self.frame.grid_rowconfigure(i, weight=1)
        rownum = 0
        Label(self.frame, text='Minimize on Save:').grid(row=rownum, sticky='w')

        # self.mos_var.set(CONFIG.get('main', 'minimize_on_save'))
        self.checkbutton_min_on_save = Checkbutton(self.frame, text='', variable=self.mos_var)
        self.checkbutton_min_on_save.grid(row=rownum, column=1, columnspan=2, sticky='w')

        rownum += 1
        Label(self.frame, text='Default Day Start:').grid(row=rownum, sticky='w')
        self.optmenu_start_work_hour = add_optmenu_start_work_hour(self.frame, self.day_start_default_var)
        self.optmenu_start_work_hour.grid(row=rownum, column=1, columnspan=2, sticky='w')

        rownum += 1
        Label(self.frame, text='Backup Every # Save:').grid(row=rownum, sticky='w')
        self.entry_backup_hist_every = Entry(self.frame, width=5)
        self.entry_backup_hist_every.grid(row=rownum, column=1, columnspan=2, sticky='w')

        rownum += 1
        Label(self.frame, text='Keep # Backups:').grid(row=rownum, sticky='w')
        self.entry_keep_backups = Entry(self.frame, width=5)
        self.entry_keep_backups.grid(row=rownum, column=1, columnspan=2, sticky='w')

        rownum += 1
        Label(self.frame, text='Config File Path:').grid(row=rownum, sticky='w')
        self.entry_confpath = Entry(self.frame)
        self.entry_confpath.insert(0, CONFIG_FPATH)
        self.entry_confpath.configure(stat='readonly')
        self.entry_confpath.grid(row=rownum, column=1, columnspan=2, sticky='we')

        rownum += 1
        Label(self.frame, text='Log File Path:').grid(row=rownum, sticky='w')
        self.entry_logpath = Entry(self.frame)
        self.entry_logpath.insert(0, LOG_FPATH)
        self.entry_logpath.configure(stat='readonly')
        self.entry_logpath.grid(row=rownum, column=1, columnspan=2, sticky='we')

        rownum += 1
        Label(self.frame, text='Ticket System URL:').grid(row=rownum, sticky='w')
        self.entry_ticket_system_url = Entry(self.frame)
        self.entry_ticket_system_url.grid(row=rownum, column=1, columnspan=2, sticky='we')

        # rownum += 1
        # Label(self.frame, text='Ticket System Auth Token:').grid(row=rownum, sticky='w')
        # self.entry_ticket_system_auth_token = Entry(self.frame)
        # self.entry_ticket_system_auth_token.grid(row=rownum, column=1, columnspan=2, sticky='we')

        rownum += 1
        Label(self.frame, text='Ticket System Username:').grid(row=rownum, sticky='w')
        self.entry_ticket_system_username = Entry(self.frame)
        self.entry_ticket_system_username.grid(row=rownum, column=1, columnspan=2, sticky='we')

        rownum += 1
        Label(self.frame, text='Ticket System Password:').grid(row=rownum, sticky='w')
        # self.entry_ticket_system_password = Entry(self.frame)
        self.entry_ticket_system_password = Entry(self.frame, show='*')
        self.entry_ticket_system_password.grid(row=rownum, column=1, columnspan=2, sticky='we')

        rownum += 1
        Label(self.frame, text='Ticket System Project Name:').grid(row=rownum, sticky='w')
        self.entry_ticket_system_project_name = Entry(self.frame)
        self.entry_ticket_system_project_name.grid(row=rownum, column=1, columnspan=2, sticky='we')

        rownum += 1
        Label(self.frame, text='Connection Test:').grid(row=rownum, sticky='w')
        self.button_ticket_system_connection_test = Button(self.frame, text='Test', command=self.cb_ticket_system_connection_test, width=15)
        self.button_ticket_system_connection_test.grid(row=rownum, column=1, sticky='w')
        self.entry_ticket_system_conntest_result = Entry(self.frame, textvariable=self.conntest_result_var, justify='center')
        self.entry_ticket_system_conntest_result.grid(row=rownum, column=2, sticky='we')

        rownum += 1
        Label(self.frame, text='Max Cat. Display Length:').grid(row=rownum, sticky='w')
        cat_disp_lens = get_category_disp_lengths(CFG_MAIN)
        self.cat_disp_optmenu = OptionMenu(self.frame, self.cat_disp_len_hist_var, *cat_disp_lens)
        self.cat_disp_optmenu.grid(row=rownum, column=1, columnspan=1, sticky='w')

        rownum += 1
        self.add_OK_Cancel(rownum, cancel_column=2)
        LOG.info('Settings dialog starting')
        self.populate_entries()

    def cb_ticket_system_connection_test(self, event=None):
        LOG.info('test')
        url = self.entry_ticket_system_url.get()
        username = self.entry_ticket_system_username.get()
        password = self.entry_ticket_system_password.get()
        project_name = self.entry_ticket_system_project_name.get()
        kb = KanboardConnection(url=url, username=username, password=password, project_name=project_name)
        response = kb.connection_test()
        project_id = response.get('project_id')
        if project_id is not None:
            self.conntest_result_var.set('Connection Successful (project_id: {})'.format(project_id))
            self.entry_ticket_system_conntest_result.configure({'background': 'lime green'})
        else:
            self.conntest_result_var.set('Connection ERROR: {}'.format(response.get('error')))
            self.entry_ticket_system_conntest_result.configure({'background': 'orange red'})
        LOG.info('Test connection result: %s', response)

    def populate_entries(self):
        self.entry_backup_hist_every.insert(0, config_setdefault('backup_hist_every'))
        self.entry_keep_backups.insert(0, config_setdefault('keep_backups'))
        self.mos_var.set(int(config_setdefault('minimize_on_save')))
        self.day_start_default_var.set(config_setdefault('work_hour_default'))
        # self.entry_ticket_system_auth_token.insert(0, config_setdefault('ticket_system_auth_token'))
        self.entry_ticket_system_url.insert(0, config_setdefault('ticket_system_url'))
        self.entry_ticket_system_username.insert(0, config_setdefault('ticket_system_username'))
        self.entry_ticket_system_password.insert(0, decrypt_str(config_setdefault('ticket_system_password')))
        self.entry_ticket_system_project_name.insert(0, config_setdefault('ticket_system_project_name'))
        self.cat_disp_len_hist_var.set(int(config_setdefault('cat_disp_len_hist')))

    def _set_config_value(self, tkelem, config_setting_name, section='main', convert_to_int=True, strip=True):
        value = tkelem
        if not isinstance(value, basestring):
            value = tkelem.get()
        if isinstance(value, basestring) and strip:
            value = value.strip()
        try:
            if convert_to_int:
                value = int(value)
            LOG.debug('config_setting_name %s value %s', config_setting_name, value)
            CONFIG.set(section, config_setting_name, str(value))
        except (ValueError, TypeError) as exc:
            LOG.warning(exc)

    def save_settings(self):
        self._set_config_value(self.entry_backup_hist_every, 'backup_hist_every')
        self._set_config_value(self.entry_keep_backups, 'keep_backups')
        self._set_config_value(self.mos_var, 'minimize_on_save')
        self._set_config_value(self.day_start_default_var, 'work_hour_default', convert_to_int=False)
        self._set_config_value(self.entry_ticket_system_url, 'ticket_system_url', convert_to_int=False)
        self._set_config_value(self.entry_ticket_system_username, 'ticket_system_username', convert_to_int=False)
        self._set_config_value(encrypt_str(self.entry_ticket_system_password.get()), 'ticket_system_password', convert_to_int=False)
        self._set_config_value(self.entry_ticket_system_project_name, 'ticket_system_project_name', convert_to_int=False)
        self._set_config_value(self.cat_disp_len_hist_var, 'cat_disp_len_hist', convert_to_int=True)
        with codecs.open(CONFIG_FPATH, 'wb', encoding='utf-8', errors='replace') as fo:
            CONFIG.write(fo)


def add_optmenu_category_fun(main_app, frame, category_var, task_timestamp=None):
    categories = main_app.timelog.get_categories()
    if not categories:
        categories = []
    if task_timestamp:
        task = main_app.timelog.get_task_by(timestamp=task_timestamp)
        if task and task.category:
            category_var.set(task.category.name)
    optmenu_cat = OptionMenu(frame, category_var, *categories)
    return optmenu_cat


class EditHistEntry(TkModalOKCancelDialog):
    '''Edit history entry in listbox'''

    def child_init(self, main_app=None):
        """Child init of EditHistEntry."""
        self.hour_var = IntVar()
        self.minute_var = IntVar()
        self.main_app = main_app
        self.history_listbox = self.main_app.history
        self.entry_idx = None
        self.entry_content = None
        self.entry_msg = None
        self.entry_dt = None
        self.category_var = StringVar()
        self.entry_date_ro_var = StringVar()
        self.task_timestamp = None
        if self.history_listbox:
            self.entry_idx = self.history_listbox.curselection()[0]
            self.entry_content = self.history_listbox.get(self.entry_idx)
            self.entry_dt, self.entry_msg = parse_hist_entry_content(self.entry_content)
            self.task_timestamp = int(datetime_to_ts(self.entry_dt))

    def root_configure(self):
        self.root.rowconfigure(0, weight=1)
        self.root.columnconfigure(0, weight=1)
        self.root.minsize(width=500, height=50)
        # self.root.configure(background='Red')

    def populate_window(self):
        colspan = 7
        self.frame.grid(sticky="nsew", columnspan=colspan, rowspan=3)
        # self.frame.grid_columnconfigure(3, weight=1)
        # self.frame.grid_columnconfigure(4, weight=1)
        self.frame.grid_rowconfigure(3, weight=1)

        rownum = 0
        self.entry = Entry(self.frame, bg=ENTRY_BG_COLOR)
        self.entry.grid(row=rownum, column=0, columnspan=7, sticky="ew")
        self.entry.focus_set()

        rownum += 1
        Label(self.frame, text='Task Date/Time:').grid(row=rownum, column=0, sticky='w')
        self.button_date_prev_day = Button(self.frame, text='<', command=self.cb_date_prev_day)
        self.button_date_prev_day.grid(row=rownum, column=1, sticky='e')
        self.entry_date_ro = Entry(self.frame, textvariable=self.entry_date_ro_var, width=22, justify='center')
        self.entry_date_ro.configure(state='readonly')
        self.entry_date_ro.grid(row=rownum, column=2)
        self.button_date_next_day = Button(self.frame, text='>', command=self.cb_date_next_day)
        self.button_date_next_day.grid(row=rownum, column=3, sticky='w')
        # Label(self.frame, text='FINISH hour:').grid(row=rownum, column=0)
        self.hour = Entry(self.frame, width=3)
        self.hour.grid(row=rownum, column=4, sticky='e')
        # Label(self.frame, text='FINISH minute:').grid(row=rownum, column=2, sticky='e')
        Label(self.frame, text=':').grid(row=rownum, column=5)
        self.minute = Entry(self.frame, width=3)
        self.minute.grid(row=rownum, column=6, sticky='w')

        rownum += 1
        Label(self.frame, text='Category:').grid(row=rownum, column=0, sticky='w')
        self.optmenu_category = self.add_optmenu_category()
        self.optmenu_category.grid(row=rownum, column=1, columnspan=6, sticky='we')

        rownum += 1
        Label(self.frame, text='New Category:').grid(row=rownum, column=0, sticky='w')
        self.new_category = Entry(self.frame)
        self.new_category.grid(row=rownum, column=1, columnspan=6, sticky='we')

        rownum += 1
        self.add_OK_Cancel(rownum, cancel_column=6)
        self.fill_entries()

    def cb_date_prev_day(self, event=None):
        # TODO
        pass

    def cb_date_next_day(self, event=None):
        # TODO
        pass

    def set_category(self, category_name=None, task_timestamp=None):
        # TODO isolate
        new_cat = self.new_category.get()
        category_id = None
        if new_cat:
            LOG.info('New category: %s', new_cat)
            category_id = self.main_app.timelog.get_category_by_name(new_cat)
            if not category_id:
                category_id = self.main_app.timelog.add_category(new_cat)
        else:
            if not category_name:
                category_name = self.category_var.get()
            category = self.main_app.timelog.get_category_by_name(category_name)
            category_id = category.id
        if category_id:
            self.set_task_category(category_id, task_timestamp=task_timestamp)

    def set_task_category(self, category_id, task_timestamp=None):
        if not task_timestamp:
            task_timestamp = self.task_timestamp
        self.main_app.timelog.set_task_category_by_task_ts_cat_id(task_timestamp, category_id)

    def add_optmenu_category(self):
        return add_optmenu_category_fun(self.main_app, self.frame, self.category_var, task_timestamp=self.task_timestamp)

    def fill_entries(self):
        self.entry.insert(0, self.entry_msg)
        self.hour.insert(0, self.entry_dt.hour)
        self.minute.insert(0, self.entry_dt.minute)
        self.entry_date_ro_var.set(dt_to_task_datestr(self.entry_dt))

    def ok_action(self):
        entry_ts = int(datetime_to_ts(self.entry_dt))
        #TODO account for work day start possibly being later than time entered for task
        edt = self.entry_dt
        self.set_category()
        try:
            new_entry_dt = datetime.datetime(year=edt.year, month=edt.month, day=edt.day,
                            hour=int(self.hour.get()), minute=int(self.minute.get()), second=edt.second)
            self.main_app.update_timelog_entry(entry_ts, int(datetime_to_ts(new_entry_dt)), self.entry.get())
            self.main_app.populate_history_listbox(predelete_all=True)
            self.main_app.timelog.periodic_backup()
        except ValueError as exc:
            LOG.warning(exc)
            tkMessageBox.showerror('Incorrect value', 'Incorrect hour or minute value entered')
            return True


class TkPersonalActivityLog(object):
    '''Personal activity log Tk app. Main window.'''

    def __init__(self, tk_root=None, backup_hist_every=3, keep_backups=5):
        self.root = tk_root
        self.timelog = TimeLog(backup_hist_every=backup_hist_every, keep_backups=keep_backups)
        self.frame = None
        self.entry = None
        self.savebutton = None
        self.savebutton_text = StringVar()
        self.minimize_on_save = int(config_setdefault('minimize_on_save'))
        self.savebutton_text_set(self.minimize_on_save)
        self.day_start_var = StringVar()
        self.day_start_var.trace('w', self.trace_day_start_var)
        self.day_start_var_do_not_upsert = False
        self.current_day_datetime = None
        self.set_current_day()
        self.category_var = StringVar()
        self.entry_date_ro_var = StringVar()
        self.set_daystart_var()
        self.optmenu_start_work_hour = None
        self.settings_button = None
        self.export_button = None
        self.history = None
        self.settings_icon_image = PhotoImage(data=SETTINGS_ICON_BASE64)
        self.test_dbpath_writable()
        self.refresh_categories_from_kanboard()

    def refresh_categories_from_kanboard(self):
        REQUEST_Q.put({'type':'update_categories'})
        REQUEST_Q.put({'type':'update_tasks'})

    def set_current_day(self):
        self.current_day_datetime = get_midnight(datetime.datetime.today())

    def configure_window(self, root):
        '''Configuring Tk window.'''
        global STATUS_BAR
        root.columnconfigure(0, weight=1)
        root.columnconfigure(1, weight=1)
        root.columnconfigure(2, weight=1)
        root.rowconfigure(0, weight=1)
        root.configure(background='SteelBlue1')

        # self.frame = Frame(root, bg='#a4f442')
        self.frame = Frame(root, bd=10)
        self.frame.grid(sticky="nsew", columnspan=3, rowspan=1)
        self.frame.grid_columnconfigure(0, weight=1)
        self.frame.grid_columnconfigure(1, weight=1)
        self.frame.grid_columnconfigure(2, weight=1)
        self.frame.grid_rowconfigure(5, weight=1)
        self.frame.winfo_toplevel().title('Personal Activity Log')

        rownum = 0
        self.entry = Entry(self.frame, bg=ENTRY_BG_COLOR)
        self.entry.grid(row=rownum, column=0, columnspan=3, sticky="ew")
        self.entry.focus_set()

        rownum += 1
        Label(self.frame, text='Category:').grid(row=rownum, column=0, sticky='w')
        self.optmenu_category = self.add_optmenu_category()
        self.optmenu_category.grid(row=rownum, column=1, columnspan=3, sticky='we')

        rownum += 1
        self.savebutton = Button(self.frame, textvariable=self.savebutton_text, fg="Black",
                                 command=self.cb_save_button)

        self.savebutton.config(width=15)
        self.savebutton.grid(row=rownum, column=1)

        self.settings_button = Button(self.frame, command=self.cb_settings_button, highlightthickness=0, bd=0,relief='ridge', highlightbackground='white', height=22)
        self.settings_button.config(image=self.settings_icon_image, highlightthickness=0, bd=0, relief='ridge', highlightbackground='white', height=22)
        self.settings_button.image = self.settings_icon_image
        self.settings_button.grid(row=rownum, column=2, sticky='e')

        self.export_button = Button(self.frame, text="Export", command=self.cb_export_button)
        self.export_button.grid(row=rownum, column=0, sticky='w')

        rownum += 1
        Label(self.frame, text='Day start:').grid(row=rownum, sticky='w')
        self.optmenu_start_work_hour = add_optmenu_start_work_hour(self.frame, self.day_start_var)
        self.optmenu_start_work_hour.grid(row=rownum, column=1)
        # self.optmenu_start_work_hour.grid(row=rownum, column=1, sticky='we')

        rownum += 1
        self.button_date_prev_day = Button(self.frame, text='<', command=self.cb_date_prev_day)
        self.button_date_prev_day.grid(row=rownum, column=0, sticky='w')
        self.entry_date_ro = Entry(self.frame, textvariable=self.entry_date_ro_var, width=22, justify='center')
        self.entry_date_ro.configure(state='readonly')
        self.entry_date_ro.grid(row=rownum, column=1)
        self.button_date_next_day = Button(self.frame, text='>', command=self.cb_date_next_day)
        self.button_date_next_day.grid(row=rownum, column=2, sticky='e')
        self.adjust_main_ui_for_date()

        rownum += 1
        self.history = Listbox(self.frame)
        self.history.grid(row=rownum, columnspan=3, sticky="nsew")
        self.history.bind('<<ListboxSelect>>', self.edit_hist_entry)

        rownum += 1
        self.status = Label(self.root, text='', bd=0, background='#f7faff', relief=SUNKEN, anchor=CENTER)
        self.status.grid(row=rownum, column=0, columnspan=3, sticky='we')
        STATUS_BAR = self.status

        root.bind('<Key>', self.autocomplete)
        root.bind('<Return>', self.cb_save_button)
        root.bind('<KP_Enter>', self.cb_save_button)
        root.bind('<Left>', self.clear_entry_selection)
        root.bind('<Right>', self.clear_entry_selection)
        root.bind('<Down>', self.clear_entry_selection)
        root.bind('<Up>', self.clear_entry_selection)
        root.bind('<Home>', self.clear_entry_selection)
        root.bind('<End>', self.clear_entry_selection)
        root.bind('<Delete>', self.clear_entry_selection)
        root.bind('<BackSpace>', self.clear_entry_selection)
        for i in range(1, 13):
            root.bind('<F{}>'.format(i), self.clear_entry_selection)
        self._on_window_ready()

    def _on_window_ready(self):
        pass

    def add_optmenu_category(self):
        return add_optmenu_category_fun(self, self.frame, self.category_var)

    def savebutton_text_set(self, value):
        if value:
            self.savebutton_text.set('Save and Minimize')
        else:
            self.savebutton_text.set('Save')

    def reconfigure_visit(self, **kwargs):
        self.minimize_on_save = kwargs.get('minimize_on_save', 0)
        LOG.debug('Trying to reconfigure, %s', self.minimize_on_save)
        self.savebutton_text_set(self.minimize_on_save)

    def cb_date_prev_day(self, event=None):
        self.change_day_datetime(-1)

    def cb_date_next_day(self, event=None):
        self.change_day_datetime(1)

    def change_day_datetime(self, days_delta=0):
        self.current_day_datetime = dt_change_day(self.current_day_datetime, days_delta)
        self.adjust_main_ui_for_date()

    def adjust_main_ui_for_date(self):
        self.entry_date_ro_var.set(dt_to_task_datestr(self.current_day_datetime))
        self.set_daystart_var(dt=self.current_day_datetime, do_not_upsert=True)
        if self.history:
            self.populate_history_listbox(predelete_all=True)

    def cb_settings_button(self, event=None):
        TkSettingsDialog(self)

    def edit_hist_entry(self, event=None):
        EditHistEntry(self, main_app=self)

    def cb_save_button(self, event=None):
        ''' Save history on button press or otherwise relevant event.'''
        entry_content = self.entry.get()
        entry_content = entry_content.strip()
        if not entry_content:
            return
        self.flash_entry_content()
        task_dt = self.get_latest_task_timestamp()
        self.timelog.add_task(entry_content, task_datetime=task_dt)
        ic(self.current_day_datetime)
        # self.set_current_day()
        # self.adjust_main_ui_for_date()
        self.populate_history_listbox()
        QR.add_one_key(entry_content)
        self.timelog.periodic_backup()

    def get_latest_task_timestamp(self):
        current_date = datetime_to_date(self.current_day_datetime)
        start_ts = dt_to_timestamp(get_midnight(current_date))
        end_ts = dt_to_timestamp(get_midnight(current_date + datetime.timedelta(days=1)))
        day_start
        if  current_date != datetime.date.today():
            task = self.timelog.get_latest_task_between_timestamps(start_ts, end_ts)
        return task

    def cb_export_button(self, event=None, save2appdata=False):
        if save2appdata:
            LOG.debug('Event %s', event)
            fname = os.path.splitext(CONFIG_FNAME)[0]
            fname = '{}.xlsx'.format(fname)
            fpath = os.path.join(APPDATA, fname)
            LOG.info('Exporting tasks to %s', fpath)
            xe = XlsxExport(self.timelog.dump_all_data_to_dict(), fpath=fpath)
            xe.xlsx_export()
        else:
            proposed_fname = 'PersonalActivityLog_{}.xlsx'.format(datetime.datetime.now().strftime('%Y-%m-%d_%H-%M'))
            fobj = tkFileDialog.asksaveasfile(mode='wb', initialfile=proposed_fname)
            if fobj is None:
                return
            LOG.info('Exporting tasks to %s', fobj.name)
            xe = XlsxExport(self.timelog.dump_all_data_to_dict(), fpath=fobj.name)
            content = xe.xlsx_export(return_content=True)
            fobj.write(content)
            fobj.close()

    def minimize_window(self):
        self.root.wm_iconify()

    def flash_entry_content(self):
        entry_content = self.entry.get()
        elen = len(entry_content)
        self.entry.config(bg='black')
        self.root.after(50, lambda: self.entry.config(bg=ENTRY_BG_COLOR))
        self.root.after(100, lambda: self.entry.config(bg='medium blue'))
        self.root.after(150, lambda: self.entry.config(bg=ENTRY_BG_COLOR))
        self.root.after(200, lambda: self.entry.config(bg='medium blue'))
        self.root.after(250, lambda: self.entry.config(bg=ENTRY_BG_COLOR))
        LOG.warn('self.minimize_on_save %s', self.minimize_on_save)
        if self.minimize_on_save:
            self.root.after(300, self.minimize_window)
        self.root.after(350, lambda: self.entry.delete(0, elen+1))

    def show_hist_entry(self, idx, ts, s, category_name=None):
        ''' Add history entry to listbox. '''
        self.history.insert(idx, render_hist_entry_content(ts, s, category_name=category_name))

    def test_dbpath_writable(self):
        ''' Check if dbpath is writable.'''
        if not os.path.isfile(DBFPATH):
            return
        if not os.access(DBFPATH, os.W_OK):
            Tk().withdraw()
            import tkMessageBox
            tkMessageBox.showerror('History file is not writable',
                                   'History file {} is not writable! Please check the attributes of that file. I have to exit.'.format(DBFPATH),
                                   icon=tkMessageBox.ERROR)
            sys.exit(1)

    def clear_entry_selection(self, _):
        ''' Clear task entry selection.'''
        self.entry.select_clear()

    def autocomplete(self, event):
        ''' Autocomplete for typed prefix.'''
        # LOG.debug('Pressed %s', repr(event.char))
        en = self.entry
        curs_pos = en.index(INSERT)
        content = en.get()
        clen = len(content)
        if curs_pos < clen and not en.select_present():
            return
        unsel = content[0:curs_pos]
        autocompl_str = QR.prefixed_alphabetic(unsel)
        if autocompl_str:
            en.select_clear()
            en.insert(curs_pos, autocompl_str[curs_pos:])
            en.select_range(curs_pos, len(autocompl_str))
            en.icursor(curs_pos)

    def populate_history_listbox(self, predelete_all=True):
        '''Read task history into listbox for display.'''
        if predelete_all:
            self.history.delete(0, END)
        day_midnight = self.current_day_datetime + datetime.timedelta(1)
        for idx, (tstamp, content, cat_name) in enumerate(self.timelog.get_tasks(num=200, earlier_or_eq=day_midnight)):
            dt = datetime.datetime.fromtimestamp(tstamp)
            self.show_hist_entry(idx, dt, content, category_name=cat_name)

    def update_timelog_entry(self, old_timestamp, new_timestamp, content):
        '''Update task by timestamp.'''
        with ContextSession(SESSION, selectonly=True) as session:
            content = strip_category(content, session)
        self.timelog.update_task(old_timestamp, new_timestamp, content)

    def set_daystart_var(self, dt=None, do_not_upsert=False):
        value = get_day_start(dt=dt)
        if do_not_upsert:
            self.day_start_var_do_not_upsert = True
        self.day_start_var.set(value)

    def trace_day_start_var(self, *args):
        if not self.day_start_var_do_not_upsert:
            upsert_day_start(self.day_start_var.get(), midnight_ts=int(datetime_to_ts(get_midnight(self.current_day_datetime))))
        self.day_start_var_do_not_upsert = False

    def mainloop(self):
        ''' Main Tk event loop.'''
        self.configure_window(self.root)
        self.populate_history_listbox()
        #TODO refactor
        QR.populate([task[1] for task in self.timelog.get_tasks(num=-1)])
        self.root.mainloop()


def flash_status_bar_message(msg):
    STATUS_BAR.config(text=msg)
    TK_ROOT.after(5000, empty_status_bar)

def empty_status_bar():
    STATUS_BAR.config(text='')


def backup_cfg_get(cfg):
    bhe, keepb = 3, 5
    try:
        bhe = int(cfg.get('backup_hist_every', 1))
    except (TypeError, ValueError):
        pass
    try:
        keepb = int(cfg.get('keep_backups', 1))
    except (TypeError, ValueError):
        pass
    return bhe, keepb


def get_kanboard_conn_and_project_id():
    url = CFG_MAIN.get('ticket_system_url')
    username = CFG_MAIN.get('ticket_system_username')
    password = decrypt_str(CFG_MAIN.get('ticket_system_password'))
    project_name = CFG_MAIN.get('ticket_system_project_name')
    kb = KanboardConnection(url=url, username=username, password=password, project_name=project_name)
    response = kb.connection_test()
    LOG.debug('response: %s', response)
    if not response.get('error'):
        print 'project_id', response.get('project_id')
        return kb, int(response.get('project_id'))
    return None, None


def job_update_categories():
    kb, project_id = get_kanboard_conn_and_project_id()
    cnt = 0
    if project_id:
        categories = kb.get_categories(project_id)
        url = kb.get_ticketsystem_api_url()
        LOG.info('CATEGORIES %s', categories)
        with ContextSession(SESSION) as session:
            for cat_dict in categories:
                name, project_id = cat_dict.get('name'), cat_dict.get('project_id')
                cat = session.query(Category).filter_by(name=name, ticket_system_url=url).one_or_none()
                if cat:
                    cat.ticket_system_project_id = str(project_id)
                else:
                    cat = Category(name=name, ticket_system_url=url, ticket_system_project_id = str(project_id))
                    LOG.info('Adding category %s', cat)
                    cnt += 1
                session.add(cat)
        return categories, cnt
    return None, None


def job_update_tasks():
    kb, project_id = get_kanboard_conn_and_project_id()
    cnt = 0
    if project_id:
        tasks = kb.get_active_tasks(project_id)
        url = kb.get_ticketsystem_api_url()
        # with ContextSession(SESSION) as session:
        #     for cat_dict in tasks:
        #         name, project_id = cat_dict.get('name'), cat_dict.get('project_id')
        #         cat = session.query(Category).filter_by(name=name, ticket_system_url=url).one_or_none()
        #         if cat:
        #             cat.ticket_system_project_id = str(project_id)
        #         else:
        #             cat = Category(name=name, ticket_system_url=url, ticket_system_project_id = str(project_id))
        #             LOG.info('Adding category %s', cat)
        #             cnt += 1
        #         session.add(cat)
        return tasks, cnt
    return None, None


def queue_processing_thread():
    while True:
        req = REQUEST_Q.get()
        if req['type'] == 'update_categories':
            LOG.debug('req %s', req)
            result, count_added = job_update_categories(*req.get('args', ()), **req.get('kwargs', {}))
            RESULT_Q.put({'result': result, 'request': req, 'type': req['type']})
            msg = 'Updated categories from Kanboard'
            if count_added:
                msg += ' (added {} categories)'.format(count_added)
            if not result:
                msg = 'Could not connect to Kanboard (to update categories)'
                LOG.warning(msg)
            flash_status_bar_message(msg)
        elif req['type'] == 'update_tasks':
            result, count_added = job_update_tasks(*req.get('args', ()), **req.get('kwargs', {}))
            RESULT_Q.put({'result': result, 'request': req, 'type': req['type']})
            msg = 'Updated tasks from Kanboard'
            if count_added:
                msg += ' (added {} tasks)'.format(count_added)
            if not result:
                msg = 'Could not connect to Kanboard (to update tasks)'
                LOG.warning(msg)
            flash_status_bar_message(msg)
        else:
            LOG.warning('Request unknown: req')
        time.sleep(2)


def refresh_config():
    global CONFIG, CFG_MAIN
    CONFIG = get_config_parser(CONFIG_FPATH)
    CFG_MAIN = CONFIG.section_as_dict('main')
    LOG.setLevel(getattr(logging, CFG_MAIN.get('log_level', 'INFO').upper()))


def main_config():
    global CONFIG, CFG_MAIN, BHE, KEEPB, CONFIG_FPATH, TICKET_SYSTEM_URL, TICKET_SYSTEM_AUTH_TOKEN
    root = Tk()
    root.minsize(width=500, height=500)
    CONFIG_FPATH = populate_config_file_if_empty(APPDATA, CONFIG_FNAME, CONFIG_TEMPLATE)
    refresh_config()
    BHE, KEEPB = backup_cfg_get(CFG_MAIN)
    LOG.info('CONFIG_FPATH %s', CONFIG_FPATH)
    # TICKET_SYSTEM_URL = CFG_MAIN.get('ticket_system_url', '')
    # TICKET_SYSTEM_AUTH_TOKEN = CFG_MAIN.get('ticket_system_auth_token', '')
    populate_work_hour_choices()
    Base.metadata.create_all(ENGINE)
    # TODO check for errors here
    thread.start_new_thread(queue_processing_thread, ())
    return root, BHE, KEEPB


# DONE implement backup of the data
# DONE implement saving to CSV/XLSX
# DONE minimize on save
# DONE settings popup
# DONE export as tkFileDialog.asksaveasfile
# DONE log to file in APPDATA
# DONE log exceptions to file
# DONE add missing values to config file
# DONE configurable save/save and minimize
# DONE save default start working hour
# DONE implement today I start working at
# DONE allow editing activity record to change name and start time
# TODO stats window
# TODO handle read_only backup file corner case
# TODO reminder aren't you forgetting to write down your activity
# DONE measure memory use on big tlog
# DONE handle appdata properly on windows
# TODO task categories
# TODO display task category in history list
# DONE bind Enter, Esc to OK, Cancel where applicable
# DONE silence error on missing backup file on rotate
# DONE Numeric keyboard Return working as Enter
# DONE change day start on changing main window date
# DONE category dropdown in main window
# TODO save task on date that is set in main window
# TODO add changing task date
# TODO change task order in export, latest at top
# TODO integrate with kanboard
# TODO enlarge Settings window
# TODO customizable category display length
# TODO switch to using username pass in Kanboard

if __name__ == '__main__':
    mkdirp(APPDATA)
    QR = QuickRecall()
    TK_ROOT, BHE, KEEPB = main_config()
    MAIN = TkPersonalActivityLog(tk_root=TK_ROOT, backup_hist_every=BHE, keep_backups=KEEPB)
    MAIN.mainloop()
