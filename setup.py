
'''
Setup for packaging Personal Activity Log app.
'''

import sys
import os

try:
    import ez_setup
    ez_setup.use_setuptools()
except ImportError:
    pass

from setuptools import setup

APP = ['personal_act_log.py']
DATA_FILES = ['--iconfile']
OSX_OPTIONS = {'py2app':
                  {'argv_emulation': False,
                   'iconfile': 'images/pal.icns'}
              }

def read(fname):
    '''Read file contents.'''
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

MAINSCRIPT = 'personal_act_log.py'


SETUP_OPTIONS = dict(
    name="Personal Activity Log",
    version="0.1.0",
    author="Marcin Krol",
    author_email="mrkafk@gmail.com",
    description=("Personal Activity Log, Tk Gui."),
    license="MIT",
    keywords="PIM",
    url="http://packages.python.org/personal_act_log",
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Applications",
        "License :: OSI Approved :: BSD License",
        ]
)

if sys.platform == 'darwin':
    SETUP_OPTIONS.update(dict(
        setup_requires=['py2app'],
        app=[MAINSCRIPT],
        # Cross-platform applications generally expect sys.argv to
        # be used for opening files.
        options=OSX_OPTIONS,
    ))
elif sys.platform == 'win32':
    SETUP_OPTIONS.update(dict(
        setup_requires=['py2exe'],
        app=[MAINSCRIPT],
    ))
else:
    SETUP_OPTIONS.update(dict(
        # Normally unix-like platforms will use "setup.py install"
        # and install the main script as such
        scripts=[MAINSCRIPT],
    ))

setup(
    **SETUP_OPTIONS
)
