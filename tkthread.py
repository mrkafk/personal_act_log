#!/usr/bin/env python
# -*- coding: UTF-8 -*-


import Tkinter as tk
import thread
from time import sleep
import datetime
import Queue

request_queue = Queue.Queue()
result_queue = Queue.Queue()

RESULT = None


def put_queue():
    request_queue.put('something {}'.format(datetime.datetime.now()))
    try:
        result = result_queue.get_nowait()
        RESULT.delete(0, 'end')
        RESULT.insert(0, result)
        print('######### result', result)
    except Queue.Empty as exc:
        print(exc)


t = None

def threadmain():
    global t, RESULT
    t = tk.Tk()
    t.configure(width=640, height=480)
    b = tk.Button(text='test', name='button', command=put_queue)
    b.place(x=0, y=0)
    RESULT = tk.Entry(width=50)
    RESULT.place(x=1, y=40)
    t.mainloop()


def foo():
    t.title("Hello world")

def bar(button_text):
    t.children["button"].configure(text=button_text)

def get_button_text():
    return t.children["button"]["text"]

def task_thread():
    while True:
        res = request_queue.get()
        with open('thread.log', 'a') as fo:
            fo.write('{}\n'.format(res))
        result_queue.put(str(res).upper())

if __name__ == '__main__':
    thread.start_new_thread(task_thread, ())
    threadmain()
