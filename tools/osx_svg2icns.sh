#!/usr/bin/env bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
SCRIPTDIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

IMGDIR="$SCRIPTDIR"/../images
IMGDIR=$(realpath "$IMGDIR")
echo "IMGDIR $IMGDIR"

cd "$IMGDIR"
find . -maxdepth 1 -type f -name "*.png" | while read x; do

    if [ ${x: -5} == ".orig" ]; then
        continue
    fi
    if [ ! -f "${x}.orig" ]; then
        cp -fv "$x" "${x}.orig"
    fi
    cp -fv "${x}.orig" "$x"
    ICON_NAME=$(basename "$x" .png)
    rm -rfv "$ICON_NAME".iconset*
    mkdir -p "$ICON_NAME".iconset

    echo "Converting $ICON_NAME"
    convert "$x" -resize   16 "$ICON_NAME".iconset/icon_16x16.png
    convert "$x" -resize   32 "$ICON_NAME".iconset/icon_16x16@2x.png
    convert "$x" -resize   32 "$ICON_NAME".iconset/icon_32x32.png
    convert "$x" -resize   64 "$ICON_NAME".iconset/icon_32x32@2x.png
    convert "$x" -resize  128 "$ICON_NAME".iconset/icon_128x128.png
    convert "$x" -resize  256 "$ICON_NAME".iconset/icon_128x128@2x.png
    convert "$x" -resize  256 "$ICON_NAME".iconset/icon_256x256.png
    convert "$x" -resize  512 "$ICON_NAME".iconset/icon_256x256@2x.png
    convert "$x" -resize  512 "$ICON_NAME".iconset/icon_512x512.png
    convert "$x" -resize 1024 "$ICON_NAME".iconset/icon_512x512@2x.png

    iconutil -c icns "$ICON_NAME".iconset
    rm -rf "$ICON_NAME".iconset*
done