#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
The MIT License (MIT)

Copyright (c) 2017 Marcin Krol <mrkafk@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

"""
Convert PNG file to GIF, then encode it with base64. This program serves
mostly converting PNG graphics to images embeddable in source code for Tkinter.PhotoImage.
"""

import sys
import os
import base64
import subprocess
import tempfile


def check_nonempty_sysargv(sargv, usage=None, errmsg=None, errstatus=1):
    if len(sargv) == 1:
        if errmsg:
            print errmsg
        if usage:
            print usage
        sys.exit(errstatus)
    return sargv[1:]


def conv2gif(png_fpath):
    if sys.platform == 'darwin' or sys.platform.startswith('linux'):
        tmpfname = tempfile.mkstemp(suffix='.gif', dir='/tmp')[1]
    else:
        tmpfname = tempfile.mkstemp(suffix='.gif')[1]
    print 'tmpfname', tmpfname
    subp_args = ['convert', png_fpath, tmpfname]
    subprocess.check_call(subp_args)
    return tmpfname


def flow():
    png_fpath = check_nonempty_sysargv(sys.argv, usage='{}\nSpecify PNG file as first argument.'.format(os.path.basename(__file__)), errmsg='No PNG file specified.')[0]
    tmpfname = conv2gif(png_fpath)
    b64fname = '{}.b64'.format(tmpfname)
    with open(tmpfname, "rb") as fobj:
        encoded_string = base64.b64encode(fobj.read())
        print
        print
        print encoded_string
        print
        print
        print 'Saving base64 encoded GIF to file: ', b64fname
        with open(b64fname, 'wb') as fobj:
            fobj.write(encoded_string)
    os.unlink(tmpfname)


if __name__ == '__main__':
    flow()
