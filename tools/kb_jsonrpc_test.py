#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
The MIT License (MIT)

Copyright (c) 2017 Marcin Krol <mrkafk@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import datetime
import os
import cPickle
import sys
import shutil
import codecs
import logging
import operator
import Queue
import thread

sys.path.insert(0, '..')

from ConfigParser import NoOptionError
from collections import OrderedDict
from datetime import timedelta

from Tkinter import Tk, Toplevel, Frame, Entry, Listbox, Button, Label, INSERT,\
                    Checkbutton, OptionMenu, IntVar, StringVar, PhotoImage, END
import tkMessageBox
import tkFileDialog

from lib.patricia import trie
from lib.pal_utils import setup_logging, mkdirp, get_config_parser,\
                   get_config_dir_portable, populate_config_file_if_empty,\
                   RedirectStdToLog, TaskStartTimeTracker,\
                   SETTINGS_ICON_BASE64, dump_attr_values, render_hist_entry_content,\
                   parse_hist_entry_content, datetime_to_ts, XlsxExport, get_midnight,\
                   ts_to_datetime, get_first_on_criterion, unicode_csv_reader,\
                   dt_to_task_datestr, dt_change_day, KanboardConnection

from sqlalchemy import create_engine, schema, types, Column, Integer, String, desc, ForeignKey
from sqlalchemy.orm import sessionmaker, relationship, joinedload
from sqlalchemy.ext.declarative import declarative_base

from dictalchemy import DictableModel, make_class_dictable


### Globals ###

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
)
LOG = logging.getLogger()

RECALL_DATA = trie('root')
QR = None
ENTRY_BG_COLOR = '#e3ecfc'


DBFNAME = 'personal_activity_log.db'
ENGINE = None
SESSION = None
DBFPATH = None
Base = None


def progfile_dir():
    return os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

MAINDIR = progfile_dir()

CONFIG_FNAME = 'PersonalActivityLog.conf'
APPDATA = get_config_dir_portable('PersonalActivityLog')
CONFIG_FPATH = None
LOG_FPATH = os.path.join(APPDATA, 'personal_activity_log.log')
TICKET_SYSTEM_URL = None
TICKET_SYSTEM_AUTH_TOKEN = None

REQUEST_Q = Queue.Queue()
RESULT_Q = Queue.Queue()


CONFIG = None
CFG_MAIN = None
# START_TRACKER = TaskStartTimeTracker()

CONFIG_TEMPLATE = '''
[main]

# Minimize on save
minimize_on_save = 1

# Backup history every n saves
backup_hist_every = 3

# When rotating backups every "backup_hist_every" save events, keep this number of copies
keep_backups = 10

log_level = INFO
'''


CONFIG_DEFAULTS = {'minimize_on_save':1, 'backup_hist_every':3, 'keep_backups':10, 'work_hour_default':'08:00'}
WORK_HOUR_CHOICES = OrderedDict()
WORK_HOUR_CHOICES_SEC = OrderedDict()


def init_sqa(savedir):
    global ENGINE, SESSION, DBFPATH, Base
    DBFPATH = os.path.join(APPDATA, DBFNAME)
    ENGINE = create_engine('sqlite:///{}'.format(DBFPATH))
    Base = declarative_base(ENGINE, cls=DictableModel)
    SESSION = sessionmaker(bind=ENGINE)

init_sqa(APPDATA)


class Category(Base):
    __tablename__ = 'category'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    tasks = relationship('Task', order_by='Category.id', back_populates='category')


class Task(Base):
    __tablename__ = 'task'
    id = Column(Integer, primary_key=True)
    timestamp = Column(Integer)
    desc = Column(String)

    category_id = Column(Integer, ForeignKey('category.id'))
    category = relationship('Category', back_populates='tasks')

    def get_start_day_midnight_ts(self):
        '''Get timestamp of midnight for the day when the task was done.'''
        return get_midnight(ts_to_datetime(self.timestamp), return_timestamp=True)


class DayStart(Base):
    __tablename__ = 'daystart'
    id = Column(Integer, primary_key=True)
    midnight_ts = Column(Integer)
    daystart_sec = Column(Integer)


class ContextSession(object):
    def __init__(self, sesmaker, selectonly=False):
        self.sesmaker = sesmaker
        self.session = None
        self.selectonly = selectonly

    def __enter__(self):
        self.session = self.sesmaker()
        return self.session

    def __exit__(self, exc_type, exc_val, exc_tb):
        if not self.selectonly:
            self.session.commit()
        self.session.close()


def upsert_task(session, task_timestamp, desc):
    task = session.query(Task).filter(Task.timestamp == task_timestamp).one_or_none()
    if not task:
        LOG.info('Adding task "%s" for timestamp %s', desc, task_timestamp)
        task = Task(timestamp=task_timestamp, desc=desc)
    else:
        LOG.info('Updating task "%s" for timestamp %s', task.desc, task.timestamp)
        task.desc = desc
    session.add(task)


def convert_pickle(pickle_fpath):
    tlog = {}
    with open(pickle_fpath, 'rb') as fo:
        tlog = cPickle.load(fo)
    with ContextSession(SESSION) as session:
        for task_timestamp, desc in tlog.items():
            upsert_task(session, task_timestamp, desc)


def import_tasks_csv(fpath):
    with codecs.open(fpath, encoding='utf8', errors='ignore') as fo:
        reader = unicode_csv_reader(fo)
        header = next(reader)
        pos_timestamp = header.index('timestamp')
        pos_desc = header.index('desc')
        with ContextSession(SESSION) as session:
            for line in reader:
                timestamp = int(line[pos_timestamp])
                desc = line[pos_desc]
                task = Task(timestamp=timestamp, desc=desc)
                session.add(task)
                print 'Adding task', timestamp, desc
        sys.exit(0)




def config_setdefault(setting_name, section='main'):
    try:
        return CONFIG.get(section, setting_name)
    except NoOptionError:
        CONFIG.set(section, setting_name, str(CONFIG_DEFAULTS.get(setting_name, 1)))
    return CONFIG.get(section, setting_name)




def get_kanboard_data():
    url = CFG_MAIN.get('ticket_system_url')
    auth_token = CFG_MAIN.get('ticket_system_auth_token')
    project_name = CFG_MAIN.get('ticket_system_project_name')
    return url, auth_token, project_name


def refresh_config():
    global CONFIG, CFG_MAIN
    CONFIG = get_config_parser(CONFIG_FPATH)
    CFG_MAIN = CONFIG.section_as_dict('main')

import pyjsonrpc


def main_config():
    global CONFIG, CFG_MAIN, BHE, KEEPB, CONFIG_FPATH, TICKET_SYSTEM_URL, TICKET_SYSTEM_AUTH_TOKEN
    CONFIG_FPATH = populate_config_file_if_empty(APPDATA, CONFIG_FNAME, CONFIG_TEMPLATE)
    refresh_config()
    # TICKET_SYSTEM_URL = CFG_MAIN.get('ticket_system_url', '')
    # TICKET_SYSTEM_AUTH_TOKEN = CFG_MAIN.get('ticket_system_auth_token', '')
    Base.metadata.create_all(ENGINE)


def jsonrpc_call():
    http_client = pyjsonrpc.HttpClient(CFG_MAIN.get('ticket_system_url'),
                                       username=CFG_MAIN.get('username'),
                                       password=CFG_MAIN.get('password'))
    print 'Result:', http_client.call('getAllCategories', 2)


def kb_client_call():
    kb = KanboardConnection(CFG_MAIN.get('ticket_system_url'), token=CFG_MAIN.get('ticket_system_auth_token'))
    print 'Result:', kb.get_categories(2)

if __name__ == '__main__':
    mkdirp(APPDATA)
    main_config()
    # jsonrpc_call()
    kb_client_call()
