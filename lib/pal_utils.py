#!/usr/bin/env python

import errno
import logging
import logging.handlers
import os
import codecs
import ConfigParser
import sys
import getpass
import datetime
import time
import json
import operator
import pprint
import csv
import re

import lib.xlsxwriter as xlsxwriter

from collections import OrderedDict, MutableMapping
from UserDict import IterableUserDict
from abc import ABCMeta, abstractmethod

from kanboard import Kanboard
from kanboard.exceptions import KanboardClientException

# REGEX_STRIP_CAT_NAME = re.compile(u'\u00AB.*\u00BB')
REGEX_STRIP_CAT_NAME = re.compile(u'\u00AB[^\u00BB]*\u00BB')

try:
    import cStringIO as StringIO
except ImportError:
    import StringIO

LOG = logging.getLogger()

hist_entry_dt_format = '%Y-%m-%d %H:%M:%S'

def mkdirp(dirp):
    ''' Equivalent of "mkdir -p" in Unix/Linux'''
    try:
        os.makedirs(dirp)
    except OSError as exc:
        if exc.errno == errno.EEXIST:
            pass
        else:
            raise
    return dirp


def setup_logging(to_fpath=None):
    ''' Setup logging. '''
    log = logging.getLogger()
    if to_fpath:
        hndl = logging.handlers.WatchedFileHandler(to_fpath)
    else:
        hndl = logging.StreamHandler()
    hndl.setFormatter(logging.Formatter(
        '%(asctime)s %(name)s:%(lineno)d %(funcName)-14s %(levelname)s | %(message)s',
        "%Y-%m-%d %H:%M:%S"
    ))
    log.addHandler(hndl)
    return log


def dump_attr_values(o):
    attrs = [x for x in dir(o) if not str(x).startswith('__')]
    # attrs = dir(o)
    attrs.sort()
    return ' '.join(['{}:{}'.format(a, getattr(o, a)) for a in attrs])


def datetime_to_ts(dt):
    return time.mktime(dt.timetuple())


def ts_to_datetime(ts):
    return datetime.datetime.fromtimestamp(ts)


def render_hist_entry_content(dt, txt, category_name=None, maxlen=10):
    s = u'{}'.format(dt.strftime('%Y-%m-%d %H:%M:%S'))
    if category_name:
        if len(category_name) > maxlen:
            category_name = '{}...'.format(category_name[:maxlen])
        s += u' \u00AB{}\u00BB'.format(category_name)
    s += u' {}'.format(txt)
    return s


def strip_category_name(s):
    return REGEX_STRIP_CAT_NAME.sub('', s)


def parse_hist_entry_content(s):
    ds, hs, txt = s.split(' ', 2)
    LOG.warning('TXT %s', txt)
    txt = strip_category_name(txt).strip()
    dt = datetime.datetime.strptime(u'{} {}'.format(ds, hs), hist_entry_dt_format)
    return dt, txt


def get_midnight(dt, return_timestamp=False):
    midnight = datetime.datetime.combine(dt, datetime.time.min)
    if return_timestamp:
        return int(datetime_to_ts(midnight))
    return midnight


def json_str_pretty(obj):
    return json.dumps(obj, indent=4)


def flatten_seq(lst):
    flat = []
    for x in lst:
        if hasattr(x, '__iter__') and not isinstance(x, basestring):
            flat.extend(flatten_seq(x))
        else:
            flat.append(x)
    return flat


def flatten_list(lst):
    flat = []
    for x in lst:
        if isinstance(x, list) and not isinstance(x, basestring):
            flat.extend(flatten_list(x))
        else:
            flat.append(x)
    return flat


class RedirectStdToLog(object):

    def __init__(self, logfun):
        self.logfun = logfun

    def write(self, msg):
        if msg.strip():
            msg = msg.rstrip('\n')
            self.logfun(msg)

    def flush(self):
        self.logfun(sys.stderr)
        self.logfun(sys.stdout)


class DictConfigParser(ConfigParser.SafeConfigParser):
    def __init__(self, *args, **kwargs):
        ConfigParser.SafeConfigParser.__init__(self, *args, **kwargs)

    def section_as_dict(self, name):
        return dict(**self._sections.get(name))


# def get_monospace_font_name():
#     names = [
#     'Hack',
#     'Andale Mono',
#     'Liberation Mono',
#     'Droid Sans Mono',
#     'DejaVu Sans Mono',
#     'Ubuntu Mono',
#     ]
#     system_families = tkFont.families()
#     for name in names:
#         if name in system_families:
#             LOG.debug('Using font %s for history listbox', name)
#             return tkFont.Font(family=name, size=13, weight='normal')
#     return 'TkFixedFont'


def get_config_parser(p):
    cp = None
    with codecs.open(p, mode='rb', encoding='utf-8', errors='replace') as fo:
        cp = DictConfigParser()
        cp.readfp(fo)
    return cp


def get_user_portable():
    return getpass.getuser()


def get_config_dir_portable(app_folder):
    username = get_user_portable()
    if sys.platform == 'darwin':
        return '/Users/{}/Library/Preferences/{}'.format(username, app_folder)
    elif sys.platform == 'win32':
        return os.path.join(os.getenv('APPDATA'), app_folder)
    else:
        pass


def utf8_file_read(fpath, errors='replace'):
    ''' Read file fpath using utf-8 encoding, return unicode.'''
    with codecs.open(fpath, mode='rb', errors=errors, encoding='utf-8') as fo:
        return fo.read()


def utf8_file_write(fpath, content_unicode, errors='replace'):
    ''' Write content_unicode to file fpath using utf-8 encoding.'''
    with codecs.open(fpath, mode='wb', errors=errors, encoding='utf-8') as fo:
        fo.write(content_unicode)


def file_content_emptyish(fpath):
    '''Check if file content contains any non-whitespace characters.'''
    content = utf8_file_read(fpath)
    content = content.replace('\n', '').strip()
    if len(content) == 0:
        return True
    return False


def populate_config_file_if_empty(appdata, config_fname, content_template):
    config_fpath = os.path.join(appdata, config_fname)
    if not os.path.isdir(appdata):
        mkdirp(appdata)
    if not os.path.isfile(config_fpath) or file_content_emptyish(config_fpath):
        utf8_file_write(config_fpath, content_template)
    return config_fpath


def timestamp_to_dt(ts):
    return datetime.datetime.fromtimestamp(ts)


def dt_to_timestamp(dt):
    return (dt - datetime.datetime(1970, 1, 1)).total_seconds()


def timedelta_to_str(tdelta):
    days = tdelta.days
    #TODO fix
    hours = tdelta.seconds/3600
    minutes = (tdelta.seconds - hours*3600)/60
    return '{}d {}h {}m'.format(days, hours, minutes)


def seconds_to_hr_min(seconds):
    hours = seconds/3600
    minutes = (seconds - hours*3600)/60
    return '{}h {}m'.format(hours, minutes)


def get_first_on_criterion(seq, criterion):
    return next(x for x in seq if criterion(x))


def split_str_list(value, sep=','):
    '''Split value by sep, return list of stripped elements.'''
    return [x.strip() for x in value.split(sep)]


def get_category_disp_lengths(cfg_main):
    cats = cfg_main.get('category_hist_display_lengths')
    if cats:
        cats = [int(x) for x in split_str_list(cats)]
    else:
        cats = [5, 10, 15, 20, 25, 30]
    return cats

def datetime_to_date(dt):
    return datetime.date(dt.year, dt.month, dt.day)

'''
Example task data dump:

{
    1504389600: [
        {
            "timestamp": 1504442229,
            "daystart_sec": 21600,
            "task_duration_sec": 31029,
            "desc": "task 1",
            "id": 80,
            "midnight_ts": 1504389600
        }
    ],
    1504908000: [
        {
            "timestamp": 1504969575,
            "daystart_sec": 21600,
            "task_duration_sec": 39975,
            "desc": "task 2",
            "id": 68,
            "midnight_ts": 1504908000
        },
        {
            "timestamp": 1504993140,
            "daystart_sec": 21600,
            "task_duration_sec": 23565,
            "desc": "task 3",
            "id": 79,
            "midnight_ts": 1504908000
        }
    ],
    1504994400: [
        {
            "timestamp": 1505062255,
            "daystart_sec": 21600,
            "task_duration_sec": 46255,
            "desc": "task 4",
            "id": 77,
            "midnight_ts": 1504994400
        },
        {
            "timestamp": 1505064346,
            "daystart_sec": 21600,
            "task_duration_sec": 2091,
            "desc": "task 1",
            "id": 35,
            "midnight_ts": 1504994400
        }
    ],
		...
}
'''

class TaskStats(object):

    def __init__(self, task_data_dump):
        self.task_data_dump = task_data_dump

    def _get_flat_task_list(self):
        return flatten_list(self.task_data_dump.values())

    def by_task_time_cumulative(self):
        tasks = self._get_flat_task_list()
        by_task_desc = OrderedDict()
        for task in tasks:
            by_task_desc.setdefault(task['desc'], []).append(task['task_duration_sec'])
        task_times = {desc:sum(by_task_desc[desc]) for desc in by_task_desc}.items()
        task_times.sort(key=operator.itemgetter(1), reverse=True)
        return task_times


class XlsxExport(object):

    def __init__(self, data_dump, fpath=None):
        self.data_dump = data_dump
        self.fpath = fpath

    def xlsx_export(self, fpath=None, return_content=False):
        LOG.debug('Export data: %s', json_str_pretty(self.data_dump))
        if not fpath:
            fpath = self.fpath
        file_like = fpath
        if return_content:
            file_like = StringIO.StringIO()
        worksheet, ws_by_time_cumul, date_format, percent_fmt, wb = self.make_workbook(file_like)
        self.write_details(worksheet, date_format)
        self.write_stats(ws_by_time_cumul, date_format, percent_fmt)
        self.add_task_time_cumul_pie_chart(wb, ws_by_time_cumul)
        wb.close()
        if return_content:
            content = file_like.getvalue()
            file_like.close()
            return content
        return file_like

    def write_stats(self, ws_by_time_cumul, date_format, percent_fmt):
        stats = TaskStats(self.data_dump)
        task_time_cumulative = stats.by_task_time_cumulative()
        total = sum([x[1] for x in task_time_cumulative])
        i = 1
        for task_desc, task_cumul_sec in task_time_cumulative:
            ws_by_time_cumul.write_string(i, 0, task_desc)
            ws_by_time_cumul.write_string(i, 1, seconds_to_hr_min(task_cumul_sec))
            ws_by_time_cumul.write_number(i, 2, task_cumul_sec)
            ws_by_time_cumul.write_number(i, 3, (1.0*task_cumul_sec)/total, percent_fmt)
            i += 1
        return task_time_cumulative

    def write_details(self, worksheet, date_format):
        i = 1
        for tasklist in self.data_dump.values():
            for task in tasklist:
                worksheet.write_datetime(i, 0, timestamp_to_dt(task['timestamp']), date_format)
                worksheet.write_number(i, 1, task['task_duration_sec'])
                worksheet.write_string(i, 2, timedelta_to_str(datetime.timedelta(seconds=task['task_duration_sec'])))
                worksheet.write_string(i, 3, task['desc'])
                i += 1

    def add_task_time_cumul_pie_chart(self, wb, ws, top_n=10, sheet_name='Cumulative Task Time'):
        chart = wb.add_chart({'type': 'pie'})
        chart.set_title({'name': 'Top {} tasks by cumulative time'.format(top_n)})
        chart.add_series({'values': '''='{}'!$D$2:$D${}'''.format(sheet_name, top_n+1),
                          'categories': '''='{}'!$A$2:$A${}'''.format(sheet_name, top_n+1),
                          'data_labels': {'category': True}})
        ws.insert_chart('F2', chart)
        # tasks = task_time_cumulative[:top_n]
        # total = sum([x[1] for x in tasks])




    def make_workbook(self, file_like):
        wb = xlsxwriter.Workbook(file_like)
        worksheet = wb.add_worksheet(name='Details')
        worksheet.set_column('A:A', 20)
        worksheet.set_column('B:B', 20)
        worksheet.set_column('C:C', 20)
        worksheet.set_column('D:D', 200)
        bold = wb.add_format({'bold': True})
        date_format = wb.add_format({'num_format': 'yyyy-mm-dd hh:mm'})
        percent_fmt = wb.add_format({'num_format': '0%'})
        worksheet.write_string(0, 0, 'Date and Time', bold)
        worksheet.write_string(0, 1, 'Duration (s)', bold)
        worksheet.write_string(0, 2, 'Duration', bold)
        worksheet.write_string(0, 3, 'Activity', bold)
        ws_by_time_cumul = wb.add_worksheet(name='Cumulative Task Time')
        ws_by_time_cumul.write_string(0, 0, 'Task', bold)
        ws_by_time_cumul.write_string(0, 1, 'Cumulative time (day:hr:min)', bold)
        ws_by_time_cumul.write_string(0, 2, 'Cumul. time (sec)', bold)
        ws_by_time_cumul.write_string(0, 3, '% Total Time Spent', bold)
        ws_by_time_cumul.set_column('A:A', 30)
        ws_by_time_cumul.set_column('B:B', 25)
        ws_by_time_cumul.set_column('C:C', 15)
        ws_by_time_cumul.set_column('D:D', 15)
        return worksheet, ws_by_time_cumul, date_format, percent_fmt, wb


class TimeLog(MutableMapping):
    """Dictionary-like class"""

    def __init__(self, *args, **kwargs):
        self._d = {}
        self._daystarts = {}
        self.update(dict(*args, **kwargs))

    def __getitem__(self, key):
        return self._d[key]

    def __setitem__(self, key, value):
        self._d[key] = value

    def __delitem__(self, key):
        del self._d[key]

    def __iter__(self):
        return iter(self._d)

    def __len__(self):
        return len(self._d)


class TaskStartTimeTracker(object):
    def __init__(self):
        self.t_start = self.reset()

    def value(self):
        return self.t_start

    def reset(self):
        self.t_start = datetime.datetime.now()
        LOG.info('Time start: %s', self.t_start)
        return self.t_start


def dt_to_task_datestr(dt):
    return dt.strftime('%a %b %d (%d/%m/%Y)')


def dt_change_day(dt, days_delta, max_today=True):
    new_datetime = dt + datetime.timedelta(days_delta)
    if max_today:
        today = datetime.datetime.today()
        if new_datetime < today:
            return new_datetime
        return today
    return new_datetime


def unicode_csv_reader(unicode_csv_data, dialect=csv.excel, **kwargs):
    # csv.py doesn't do Unicode; encode temporarily as UTF-8:
    csv_reader = csv.reader(utf_8_encoder(unicode_csv_data),
                            dialect=dialect, **kwargs)
    for row in csv_reader:
        # decode UTF-8 back to Unicode, cell by cell:
        yield [unicode(cell, 'utf-8') for cell in row]


def utf_8_encoder(unicode_csv_data):
    for line in unicode_csv_data:
        yield line.encode('utf-8')


class TicketSystem:

    __metaclass__ = ABCMeta

    @abstractmethod
    def get_categories(self):
        pass

    @abstractmethod
    def get_active_tasks(self):
        pass

    @abstractmethod
    def set_ticketsystem_api_url(self, url):
        pass

    @abstractmethod
    def get_ticketsystem_api_url(self, url):
        pass

    @abstractmethod
    def set_login_details(self, username=None, password=None, auth_token=None):
        pass

    @abstractmethod
    def set_project_name(self, project_name):
        pass

    @abstractmethod
    def connection_test(self):
        pass


class KanboardConnection(TicketSystem):

    def __init__(self, url=None, username=None, password=None, project_name=None):
        self.url = None
        self.set_ticketsystem_api_url(url)
        self.username = None
        self.password = None
        self.set_login_details(username=username, password=password)
        self.project_name = None
        self.set_project_name(project_name)
        self.kb_client = None

    def create_kb_client(self):
        self.kb_client = Kanboard(self.url, self.username, self.password)

    def get_categories(self, project_id):
        if not self.kb_client:
            self.create_kb_client()
        categories = self.kb_client.get_all_categories(project_id=project_id)
        return categories

    def get_active_tasks(self, project_id):
        if not self.kb_client:
            self.create_kb_client()
        tasks = self.kb_client.get_all_tasks(project_id=project_id, status_id=1)
        LOG.debug('Ticket system tasks: %s', tasks[:10])
        return tasks

    def set_ticketsystem_api_url(self, url):
        self.url = url

    def get_ticketsystem_api_url(self):
        return self.url

    def set_login_details(self, username=None, password=None):
        self.username = username
        self.password = password

    def set_project_name(self, project_name):
        self.project_name = project_name

    def get_project_by_name(self, name):
        return self.kb_client.get_project_by_name(name=name)

    def connection_test(self):
        if not self.kb_client:
            self.create_kb_client()
        LOG.debug('project_name %s url %s', self.project_name, self.url)
        conn_data = OrderedDict()
        try:
            response = self.get_project_by_name(self.project_name)
        except KanboardClientException as exc:
            return {'error': str(exc)}
        conn_data['project_id'] = response.get('id')
        conn_data['email'] =  response.get('email')
        conn_data['board'] = response.get('url').get('board')
        return conn_data


SETTINGS_ICON_BASE64 = '''R0lGODlhEwASAPMAALa4u7q8v7/Aw8PFxsvNzszOz9DQ0tTV19jZ29zd3+Dh4/Lz9PX29/r7/AAAAAAAACH5BAAAAAAAIf8LSUNDUkdCRzEwMTL/AAAOWGFwcGwCEAAAbW50clJHQiBYWVogB+AACwAaABMABQAjYWNzcEFQUEwAAAAAQVBQTAAAAAAAAAAAAAAAAAAAAAAAAPbWAAEAAAAA0y1hcHBsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARZGVzYwAAAVAAAABiZHNjbQAAAbQAAAM+Y3BydAAABPQAAAAjd3RwdAAABRgAAAAUclhZWgAABSwAAAAUZ1hZWgAABUAAAAAUYlhZWgAABVQAAAAUclRSQwAABWgAAAgMYWFyZwAADXQAAAAgdmNndAAADZQAAAAwbmRp/24AAA3EAAAAPmNoYWQAAA4EAAAALG1tb2QAAA4wAAAAKGJUUkMAAAVoAAAIDGdUUkMAAAVoAAAIDGFhYmcAAA10AAAAIGFhZ2cAAA10AAAAIGRlc2MAAAAAAAAACERpc3BsYXkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABtbHVjAAAAAAAAACIAAAAMaHJIUgAAAAwAAAGoa29LUgAAAAYAAAG0bmJOTwAAAAwAAAG6aWQAAAAAAAoAAAHGaHVIVQAAABgAAP8B0GNzQ1oAAAAOAAAB6GRhREsAAAAKAAAB9nVrVUEAAAAOAAACAGFyAAAAAAAUAAACDml0SVQAAAAOAAAB6HJvUk8AAAAMAAACIm5sTkwAAAAoAAACLmhlSUwAAAAGAAACVmVzRVMAAAAQAAACXGZpRkkAAAAMAAACbHpoVFcAAAAGAAACeHZpVk4AAAAQAAACfnNrU0sAAAAOAAACjnpoQ04AAAAGAAACnHJ1UlUAAAAOAAAComZyRlIAAAAwAAACsG1zAAAAAAAOAAAC4GNhRVMAAAAQAAACXHRoVEgAAAAQAAAC7mVzWEwAAAAQAAACXGRlREUAAAAOAAAB6GX/blVTAAAADgAAAv5wdEJSAAAADgAAAehwbFBMAAAADgAAAehlbEdSAAAACgAAAwxzdlNFAAAAEgAAAxZ0clRSAAAACgAAAyhqYUpQAAAADAAAAzJwdFBUAAAADgAAAegAWgBhAHMAbABvAG66qLLI0TAAUwBrAGoAZQByAG0ATABhAHkAYQByAE0AZQBnAGoAZQBsAGUAbgDtAHQA6QBzAE0AbwBuAGkAdABvAHIAUwBrAOYAcgBtBBQEOARBBD8EOwQ1BDkGNAYnBjQGKQAgBicGRAY5BjEGNgBBAGYAaQIZAGEAagBPAG4AYgBlAGsAZQBuAGQAIABiAGUAZQBs/wBkAHMAYwBoAGUAcgBtBd4F4QXaAFAAYQBuAHQAYQBsAGwAYQBOAOQAeQB0AHQA9phveTpWaABIAGkewwBuACAAdABoHssARABpAHMAcABsAGUAamY+eTpWaAQcBD4EPQQ4BEIEPgRAAE0AbwBuAGkAdABlAHUAcgAgAGQAZQAgAHQAeQBwAGUAIABpAG4AYwBvAG4AbgB1AFAAYQBwAGEAcgBhAG4OCA4tDkEOKg4UDgcOHA4lAEQAaQBzAHAAbABhAHkDnwO4A8wDvQO3AEIAaQBsAGQAcwBrAOQAcgBtAEUAawByAGEAbjDHMKMwuTDXMOwwpAAAdGV4dAAAAP8AQ29weXJpZ2h0IEFwcGxlIEluYy4sIDIwMTYAAFhZWiAAAAAAAADz2AABAAAAARYIWFlaIAAAAAAAAKGcAABMMwAAANtYWVogAAAAAAAAL44AAKI9AAAUjlhZWiAAAAAAAAAlrAAAEZAAAL3EY3VydgAAAAAAAAQAAAAABQAKAA8AFAAZAB4AIwAoAC0AMgA2ADsAQABFAEoATwBUAFkAXgBjAGgAbQByAHcAfACBAIYAiwCQAJUAmgCfAKMAqACtALIAtwC8AMEAxgDLANAA1QDbAOAA5QDrAPAA9gD7AQEBBwENARMBGQEfASUBKwEyATgBPgFFAUwBUgFZAWD/AWcBbgF1AXwBgwGLAZIBmgGhAakBsQG5AcEByQHRAdkB4QHpAfIB+gIDAgwCFAIdAiYCLwI4AkECSwJUAl0CZwJxAnoChAKOApgCogKsArYCwQLLAtUC4ALrAvUDAAMLAxYDIQMtAzgDQwNPA1oDZgNyA34DigOWA6IDrgO6A8cD0wPgA+wD+QQGBBMEIAQtBDsESARVBGMEcQR+BIwEmgSoBLYExATTBOEE8AT+BQ0FHAUrBToFSQVYBWcFdwWGBZYFpgW1BcUF1QXlBfYGBgYWBicGNwZIBlkGagZ7BowGnQavBsAG0QbjBvUHBwcZBysHPQdPB2EHdAeGB5kH/6wHvwfSB+UH+AgLCB8IMghGCFoIbgiCCJYIqgi+CNII5wj7CRAJJQk6CU8JZAl5CY8JpAm6Cc8J5Qn7ChEKJwo9ClQKagqBCpgKrgrFCtwK8wsLCyILOQtRC2kLgAuYC7ALyAvhC/kMEgwqDEMMXAx1DI4MpwzADNkM8w0NDSYNQA1aDXQNjg2pDcMN3g34DhMOLg5JDmQOfw6bDrYO0g7uDwkPJQ9BD14Peg+WD7MPzw/sEAkQJhBDEGEQfhCbELkQ1xD1ERMRMRFPEW0RjBGqEckR6BIHEiYSRRJkEoQSoxLDEuMTAxMjE0MTYxODE6QTxRPlFAYUJxRJFGoUi/8UrRTOFPAVEhU0FVYVeBWbFb0V4BYDFiYWSRZsFo8WshbWFvoXHRdBF2UXiReuF9IX9xgbGEAYZRiKGK8Y1Rj6GSAZRRlrGZEZtxndGgQaKhpRGncanhrFGuwbFBs7G2MbihuyG9ocAhwqHFIcexyjHMwc9R0eHUcdcB2ZHcMd7B4WHkAeah6UHr4e6R8THz4faR+UH78f6iAVIEEgbCCYIMQg8CEcIUghdSGhIc4h+yInIlUigiKvIt0jCiM4I2YjlCPCI/AkHyRNJHwkqyTaJQklOCVoJZclxyX3JicmVyaHJrcm6CcYJ0kneierJ9woDSg/KHEooijUKQYpOCn/aymdKdAqAio1KmgqmyrPKwIrNitpK50r0SwFLDksbiyiLNctDC1BLXYtqy3hLhYuTC6CLrcu7i8kL1ovkS/HL/4wNTBsMKQw2zESMUoxgjG6MfIyKjJjMpsy1DMNM0YzfzO4M/E0KzRlNJ402DUTNU01hzXCNf02NzZyNq426TckN2A3nDfXOBQ4UDiMOMg5BTlCOX85vDn5OjY6dDqyOu87LTtrO6o76DwnPGU8pDzjPSI9YT2hPeA+ID5gPqA+4D8hP2E/oj/iQCNAZECmQOdBKUFqQaxB7kIwQnJCtUL3QzpDfUPARANER0SKRM5FEkVVRZpF3kYiRmdGq0bw/0c1R3tHwEgFSEtIkUjXSR1JY0mpSfBKN0p9SsRLDEtTS5pL4kwqTHJMuk0CTUpNk03cTiVObk63TwBPSU+TT91QJ1BxULtRBlFQUZtR5lIxUnxSx1MTU19TqlP2VEJUj1TbVShVdVXCVg9WXFapVvdXRFeSV+BYL1h9WMtZGllpWbhaB1pWWqZa9VtFW5Vb5Vw1XIZc1l0nXXhdyV4aXmxevV8PX2Ffs2AFYFdgqmD8YU9homH1YklinGLwY0Njl2PrZEBklGTpZT1lkmXnZj1mkmboZz1nk2fpaD9olmjsaUNpmmnxakhqn2r3a09rp2v/bFdsr20IbWBtuW4Sbv9rbsRvHm94b9FwK3CGcOBxOnGVcfByS3KmcwFzXXO4dBR0cHTMdSh1hXXhdj52m3b4d1Z3s3gReG54zHkqeYl553pGeqV7BHtje8J8IXyBfOF9QX2hfgF+Yn7CfyN/hH/lgEeAqIEKgWuBzYIwgpKC9INXg7qEHYSAhOOFR4Wrhg6GcobXhzuHn4gEiGmIzokziZmJ/opkisqLMIuWi/yMY4zKjTGNmI3/jmaOzo82j56QBpBukNaRP5GokhGSepLjk02TtpQglIqU9JVflcmWNJaflwqXdZfgmEyYuJkkmZCZ/JpomtWbQpuvnByciZz3nWSd0p5Anq6fHZ+Ln/r/oGmg2KFHobaiJqKWowajdqPmpFakx6U4pammGqaLpv2nbqfgqFKoxKk3qamqHKqPqwKrdavprFys0K1ErbiuLa6hrxavi7AAsHWw6rFgsdayS7LCszizrrQltJy1E7WKtgG2ebbwt2i34LhZuNG5SrnCuju6tbsuu6e8IbybvRW9j74KvoS+/796v/XAcMDswWfB48JfwtvDWMPUxFHEzsVLxcjGRsbDx0HHv8g9yLzJOsm5yjjKt8s2y7bMNcy1zTXNtc42zrbPN8+40DnQutE80b7SP9LB00TTxtRJ1MvVTtXR1lXW2Ndc1+DYZNjo2WzZ8dp22vvbgNwF3Ird/xDdlt4c3qLfKd+v4DbgveFE4cziU+Lb42Pj6+Rz5PzlhOYN5pbnH+ep6DLovOlG6dDqW+rl63Dr++yG7RHtnO4o7rTvQO/M8Fjw5fFy8f/yjPMZ86f0NPTC9VD13vZt9vv3ivgZ+Kj5OPnH+lf65/t3/Af8mP0p/br+S/7c/23//3BhcmEAAAAAAAMAAAACZmYAAPKnAAANWQAAE9AAAAoOdmNndAAAAAAAAAABAAEAAAAAAAAAAQAAAAEAAAAAAAAAAQAAAAEAAAAAAAAAAQAAbmRpbgAAAAAAAAA2AACsgAAAUcAAADAAAAC0wAAAJgAAABCAAABQQAAAVEAAAmYzMwACMzMAAjMzAAAAAAAAAABzZjMyAAAAAAABC7cAAAWW///zVwAABykAAP3X///7t////aYAAAPaAADA9m1tb2QAAAAAAAA2mAAACgNRbOan0r3soAAAAAAAAAAAAAAAAAAAAAAALAAAAAATABIAAARnsMlJq70467wAYBuzSImXSAtYMQNgHIEXHAUwqBMTe3wvXAgeIZEg8BQXgwc5UXgMlUTLZpkKDpMetKLkTQ7TQdUzOFG6TNLz4vQUiDWTRderAwI4CcsFkx2UN3MjDSUAZikhHnkUEQA7'''
